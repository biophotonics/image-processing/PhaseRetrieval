% function Phaseretrieval
% This function carries out phase retrieval of spatially coherent
% electromagnetic radiation. The input is a MATLAB .mat file with camera 2D
% measurements of the intensity (irradiance) in multiple transverse planes
% at various distances to a focus, similar to the measurements one would
% take to measure the beam quality M2. The output is another MATLAB .mat
% file containing the phase retrieved complex E-field that best fits, when
% numerically propagated, all of the input planes' intensity profiles. The
% method will only work if there are some input frames within roughly a
% Rayleigh range of the focus and some at least a few Rayleigh ranges away,
% ideally both before and after the focus.
% 
% The input .mat file must contain the following variables:
% 
% lambda,        a scalar, the wavelength measured in meters.
% z_positions,   a 1D row vector of length Nz that contains the z
%                positions of the frames, measured in meters. Only the
%                relative positions matter.
% I,             a 3D array of dimensions Nx x Ny x Nz that contains the
%                intensity measurements in the Nz different planes. You
%                must ensure that background has been carefully subtracted
%                in I.
% pixelpitch_x   a scalar, the pixel pitch in the x direction measured in
%                meters. It is equal to the detector x size divided by the
%                x resolution.
% pixelpitch_y   same as pixelpitch_x but for y
% 
% If you're recording data with a Spiricon M2-200s camera system, the
% script in the Parse_m2scordata.m file can be used to extract the above
% data from Spiricon's proprietary file format and save it in the required
% .mat file format. Otherwise, you need to create the .mat file yourself.
% 
% The two main settings to tweak to find the optimal phase retrieval are
% resizefactor and gainfactor.

% Speed test results (without plotting):
% CPU (Intel Core i5 9400F) mex function implementation is 2.6x faster than the CPU MATLAB implementation.
% Using GPU (GeForce RTX 2070) is a further 3.5x faster (9.1x faster than the CPU MATLAB implementation).

%% User specified algorithm parameters:
calcType = 2; % 1: MATLAB only, 2: MATLAB with mex function
useGPU = false; % If true, uses CUDA GPU acceleration, speeding up the calculations. Requires Nvidia GPU and Parallel Computing Toolbox
useAdaptiveXYshifting = true; % If true, will enable the algorithm to shift the measured intensity profiles slightly every 5 iterations to make them fit better with the phase retrieval.

resizefactor = 1/2; % Should be between 0 and 1. Factor by which the resolution of the input I arrays is reduced. Lower factor means lower resolution used in the phase retrieval process and faster execution time.
gainfactor = 1.4; % Should be between 1 and 1.4. Higher gain means more faint beam features are retrieved, but more noise is also introduced in the result.

ignoredframes = []; % This array with indexes of frames specify which frames should *not* be used for the phase retrieval process. They are, however, still included in the calculation of fidelities so you can see how well the phsae retrieved result fits with the omitted frames. Set to empty array ( [] ) to use all frames.
refplaneposition = 300e-3; % [m] z position of reference plane, does not affect the phase retrieval algorithm. You could for example choose this to be the focal plane. The saved E_ref array will be the complex E field in this plane.
padratio = 1/20; % Should be between 0 and 1. Padding to add on each side in units of the full image width. If you have frames in which your beam fills up most of it, ensure that this is large enough for a converged result. Lower means faster execution, higher means more accurate result.
convergencecriterion = 1e-6; % When the root mean square of all pixels' magnitudes of E field changes from one iteration to the next, relative to the max value of the magnitude of the E field, goes under this value, the phase retrieval stops because convergence is reached.

%% User specified plotting and output parameters:
saveData = true; % If true, saves output to a .mat file
plotProgress = true; % If true, will show plot groups 1 and 2 showing the convergence. Takes up a lot of computation time because MATLAB plotting is slow.
calcFidelities = true; % Calculate and show fidelities. Very slow in the non-mex implementation.
fignum1 = 1; % MATLAB figure number for progress plot group 1
fignum2 = 2; % MATLAB figure number for progress plot group 2
fignum3 = 3; % MATLAB figure for plotting final fidelities. Always shown regardless of plotProgress.
filterwidth = 1/250; % Width of Gaussian blurring filter in units of full image width, used only to help determine x and y limits for plotting. Does not affect the phase retrieval.
thresholdval = -2.5; % Logarithmic intensity threshold value to determine what x and y limits to use for plotting. Does not affect the phase retrieval.

%% It shouldn't be necessary to change anything below this line

%% Import data
[FileName,PathName] = uigetfile('*.mat','Select the .mat file');
fprintf('Loading %s...\n',[PathName FileName]);
load([PathName FileName],'I','z_positions','lambda','pixelpitch_x','pixelpitch_y');
I = double(I); lambda = double(lambda); z_positions = double(z_positions); % Convert to doubles in case they weren't already

[Nx_original,Ny_original,Nz] = size(I);
iz_PR_array = int32(setdiff(1:Nz, ignoredframes)-1); % - 1 to convert to C style indexing
Nz_PR = numel(iz_PR_array); % Number of frames in subset used for Phase Retrieval
z_positions_PR = z_positions(iz_PR_array+1);

padding = round(Nx_original*resizefactor*padratio);
I = padarray(imresize(I,resizefactor,'method','box'),padding*[1 1],0,'both');
[Nx,Ny,~] = size(I);

%% Initialize vector spaces, measured E field magnitudes and calculate the propagation kernels that will be used
dx = pixelpitch_x/resizefactor; % [m] Pixel pitch in x direction
dy = pixelpitch_y/resizefactor; % [m] Pixel pitch in y direction
Lx = dx*Nx;
Ly = dy*Ny;

kx = 2*pi/Lx*(-Nx/2:Nx/2-1);
ky = 2*pi/Ly*(-Ny/2:Ny/2-1);
[kX,kY] = ndgrid(kx,ky);

E_mag_meas = zeros(Nx,Ny,Nz_PR,'single');
prop_kernel = complex(zeros(Nx,Ny,Nz,'single'));
for fidx_PR=1:Nz_PR
  E_mag_meas(:,:,fidx_PR) = sqrt(max(0,I(:,:,iz_PR_array(fidx_PR)+1)/sum(sum(I(:,:,iz_PR_array(fidx_PR)+1)))));
end
fft_I_conj = conj(complex(single(fft2(I))));
for fidx=1:Nz
  fft_I_conj(:,:,fidx) = fft_I_conj(:,:,fidx)/sqrt(sum(sum(single(I(:,:,fidx)).^2)));
  prop_kernel(:,:,fidx) = ifftshift(exp(-1i*(z_positions(fidx)-refplaneposition)*(kX.^2 + kY.^2)*lambda/(4*pi))); % Fresnel propagation kernel
end

%% Initialize reference plane, using the closest measured plane amplitude and a flat phase front as initial guess
[~,minidx_PR] = min(abs(z_positions_PR-refplaneposition));
E_ref = complex(E_mag_meas(:,:,minidx_PR));
maxAbsEmid = max(max(E_mag_meas(:,:,minidx_PR))); % For the normalization of the convergence criterion

%% Figure initialization
if plotProgress
  x = dx*(-Nx/2:Nx/2-1);
  y = dy*(-Ny/2:Ny/2-1);

  minidx = iz_PR_array(minidx_PR)+1;
  maxAbsEbeg = max(max(E_mag_meas(:,:,1)));
  logMaxIbeg = 2*log10(maxAbsEbeg);
  logMaxImid = 2*log10(maxAbsEmid);
  maxAbsEend = max(max(E_mag_meas(:,:,end)));
  logMaxIend = 2*log10(maxAbsEend);

  figure(fignum1);clf;
  bgcolor = [0 0 0];
  caxes = [-4 0];
  sigma = Nx_original*filterwidth*resizefactor;

  Ibeg = 2*log10(E_mag_meas(:,:,1)) - logMaxIbeg;
  Ifilt = 2*log10(imgaussfilt(E_mag_meas(:,:,1),sigma)).';
  threshold = max(Ifilt(:))+thresholdval;
  x_mid = (max(x(any(Ifilt > threshold  ))) + min(x(any(Ifilt > threshold  ))))/2;
  x_span = max(x(any(Ifilt > threshold  ))) - min(x(any(Ifilt > threshold  )));
  y_mid = (max(y(any(Ifilt > threshold,2))) + min(y(any(Ifilt > threshold,2))))/2;
  y_span = max(y(any(Ifilt > threshold,2))) - min(y(any(Ifilt > threshold,2)));
  span = max(x_span,y_span);

  h_ax1 = subplot(3,3,1);
  h_im1 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis([-pi pi]);
  h_ax1.Color = bgcolor;
  h_im1.AlphaData = zeros(Ny,Nx);
  colormap(h_ax1,hsv);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved phase at z = ' num2str(z_positions_PR(1)*1e3) ' mm']);

  h_ax2 = subplot(3,3,2);
  h_im2 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax2,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved log_{10}(I) at z = ' num2str(z_positions_PR(1)*1e3) ' mm']);

  h_ax3 = subplot(3,3,3);
  imagesc(x,y,Ibeg.');
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax3,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Measured log_{10}(I) at z = ' num2str(z_positions_PR(1)*1e3) ' mm']);

  Imid = 2*log10(E_mag_meas(:,:,minidx_PR))-logMaxImid;
  Ifilt = 2*log10(imgaussfilt(E_mag_meas(:,:,minidx_PR),sigma)).';
  threshold = max(Ifilt(:))+thresholdval;
  x_mid = (max(x(any(Ifilt > threshold  ))) + min(x(any(Ifilt > threshold  ))))/2;
  x_span = max(x(any(Ifilt > threshold  ))) - min(x(any(Ifilt > threshold  )));
  y_mid = (max(y(any(Ifilt > threshold,2))) + min(y(any(Ifilt > threshold,2))))/2;
  y_span = max(y(any(Ifilt > threshold,2))) - min(y(any(Ifilt > threshold,2)));
  span = max(x_span,y_span);

  h_ax4 = subplot(3,3,4);
  h_im4 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis([-pi pi]);
  h_ax4.Color = bgcolor;
  h_im4.AlphaData = zeros(Ny,Nx);
  colormap(h_ax4,hsv);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved phase at z = ' num2str(z_positions_PR(minidx_PR)*1e3) ' mm']);

  h_ax5 = subplot(3,3,5);
  h_im5 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax5,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved log_{10}(I) at z = ' num2str(z_positions_PR(minidx_PR)*1e3) ' mm']);

  h_ax6 = subplot(3,3,6);
  imagesc(x,y,Imid.');
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax6,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Measured log_{10}(I) at z = ' num2str(z_positions_PR(minidx_PR)*1e3) ' mm']);
  
  Iend = 2*log10(E_mag_meas(:,:,end))-logMaxIend;
  Ifilt = 2*log10(imgaussfilt(E_mag_meas(:,:,end),sigma)).';
  threshold = max(Ifilt(:))+thresholdval;
  x_mid = (max(x(any(Ifilt > threshold  ))) + min(x(any(Ifilt > threshold  ))))/2;
  x_span = max(x(any(Ifilt > threshold  ))) - min(x(any(Ifilt > threshold  )));
  y_mid = (max(y(any(Ifilt > threshold,2))) + min(y(any(Ifilt > threshold,2))))/2;
  y_span = max(y(any(Ifilt > threshold,2))) - min(y(any(Ifilt > threshold,2)));
  span = max(x_span,y_span);

  h_ax7 = subplot(3,3,7);
  h_im7 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis([-pi pi]);
  h_ax7.Color = bgcolor;
  h_im7.AlphaData = zeros(Ny,Nx);
  colormap(h_ax7,hsv);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved phase at z = ' num2str(z_positions_PR(end)*1e3) ' mm']);

  h_ax8 = subplot(3,3,8);
  h_im8 = imagesc(x,y,-Inf(Ny,Nx));
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax8,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Phaseretrieved log_{10}(I) at z = ' num2str(z_positions_PR(end)*1e3) ' mm']);

  h_ax9 = subplot(3,3,9);
  imagesc(x,y,Iend.');
  axis xy
  axis equal
  axis tight
  xlim(x_mid + span*[-1 1]);
  ylim(y_mid + span*[-1 1]);
  caxis(caxes);
  colormap(h_ax9,GPBGYRcolormap);
  colorbar;
  xlabel('x [m]');
  ylabel('y [m]');
  title(['Measured log_{10}(I) at z = ' num2str(z_positions_PR(end)*1e3) ' mm']);


  figure(fignum2);clf;
  subplot(4,1,1);
  h_graph = plot(NaN(2,4),'LineWidth',2);
  grid on; grid minor;
  legend('d_x','d_x cropped','d_y','d_y cropped','Location','northeast');
  xlabel('Iteration #');
  ylabel('4\sigma width [m]');
  title('Cropped and uncropped diameter convergence');

  h_ax10 = subplot(4,1,2);
  h_im10 = imagesc(zeros(Nz,1));
  caxis([0 1]);
  colormap(h_ax10,GPBGYRcolormap);
  colorbar;
  xlabel('Iteration #');
  ylabel('Frame');
  title('Attenuation');

  subplot(4,1,3);
  h_graph2 = semilogy(NaN,'LineWidth',2);
  grid on; grid minor;
  ylim([convergencecriterion 1e-1]);
  xlabel('Iteration #');
  ylabel('RMS of abs(Delta E / max(E))');

  h_ax11 = subplot(4,1,4);
  h_im11 = imagesc(zeros(Nz,1));
  caxis([0.9 1]);
  colormap(h_ax11,GPBGYRcolormap);
  colorbar;
  xlabel('Iteration #');
  ylabel('Frame');
  title('Fidelity (max squared norm of xcorrelation coeffs)');
end

%% Initialize parameters struct
frameshifts = zeros(2,Nz_PR,'int32');
parameters = struct('plotProgress',plotProgress,'gain',single(gainfactor),'E_mag_meas',E_mag_meas,'prop_kernel',complex(prop_kernel),'fft_I_conj',complex(fft_I_conj),'iz_PR_array',iz_PR_array,'shiftframes',false);

%% Initialize and run core loop
magEdiffRMS = Inf;
iteration = 1;
fprintf('Phaseretrieving...\n');
tic

while magEdiffRMS/maxAbsEmid > convergencecriterion
  parameters.shiftframes = useAdaptiveXYshifting && rem(iteration,5) == 0 && iteration <= 50;
  if calcType == 2
    if useGPU
      [E_ref,phaseRMS,magnitudeRMS,magEdiffRMS,attenuations,frameshifts,fidelities] = PRiterator_CUDA(E_ref,parameters,frameshifts);
    else
      [E_ref,phaseRMS,magnitudeRMS,magEdiffRMS,attenuations,frameshifts,fidelities] = PRiterator(E_ref,parameters,frameshifts);
    end
  else
    E_ref_freqspace = fft2(E_ref);
    E_ref_freqspace_new = complex(zeros(Nx,Ny));
    E_ref_freqspace = E_ref_freqspace*parameters.gain;
    P_ref = sum(sum(abs(E_ref_freqspace).^2))/Nx/Ny;
    for fidx_PR=1:Nz_PR
      E_frame = ifft2(E_ref_freqspace.*prop_kernel(:,:,iz_PR_array(fidx_PR)+1));
      if calcFidelities || (rem(iteration,20) == 0 && useAdaptiveXYshifting)
        xcorrmatrix = normxcorr2(abs(E_frame).^2,E_mag_meas(:,:,fidx_PR).^2);
      end
      if calcFidelities
        fidelities(fidx_PR) = max(abs(xcorrmatrix(:))).^2;
      else
        fidelities(fidx_PR) = NaN;
      end
      if rem(iteration,20) == 0 && useAdaptiveXYshifting
        [xidx,yidx] = find(xcorrmatrix == max(xcorrmatrix(:)));
        E_mag_meas(:,:,fidx_PR) = imtranslate(E_mag_meas(:,:,fidx_PR),-2*[yidx - Ny , xidx - Nx]); % Factor of 2 is because the cross correlation tends to underestimate the shift that is eventually needed
      end
      E_frame = min(abs(E_frame), E_mag_meas(:,:,fidx_PR)).*exp(1i*angle(E_frame));
      attenuations(fidx_PR) = 1 - sum(sum(abs(E_frame).^2))/P_ref;
      E_ref_freqspace_new = E_ref_freqspace_new + fft2(E_frame)./prop_kernel(:,:,iz_PR_array(fidx_PR)+1)/Nz_PR;
    end
    E_ref_freqspace = E_ref_freqspace_new;

    E_ref_new = ifft2(E_ref_freqspace);
    magEdiffRMS = sqrt(mean((abs(E_ref_new(:) - E_ref(:))).^2));
    E_ref = E_ref_new;
  end
  
  if plotProgress
    h_im10.CData(:,iteration) = attenuations;
    h_im10.Parent.XLim = [0.5 iteration+0.5];

    E1 = ifft2(fft2(E_ref).*prop_kernel(:,:,iz_PR_array(1)+1)).';
    E2 = ifft2(fft2(E_ref).*prop_kernel(:,:,iz_PR_array(minidx_PR)+1)).';
    E3 = ifft2(fft2(E_ref).*prop_kernel(:,:,iz_PR_array(end)+1)).';
    h_im2.CData = 2*log10(abs(E1))-logMaxIbeg;
    h_im5.CData = 2*log10(abs(E2))-logMaxImid;
    h_im8.CData = 2*log10(abs(E3))-logMaxIend;
    h_im1.CData = angle(E1);
    h_im1.AlphaData = h_im2.CData > -3;
    h_im4.CData = angle(E2);
    h_im4.AlphaData = h_im5.CData > -3;
    h_im7.CData = angle(E3);
    h_im7.AlphaData = h_im8.CData > -3;
    
    xcropidxs = round(Nx_original/10*resizefactor+padding):round(Nx_original*9/10*resizefactor+padding);
    ycropidxs = round(Ny_original/10*resizefactor+padding):round(Ny_original*9/10*resizefactor+padding);
    h_graph(1).YData(iteration) = 4*std(x,sum(abs(E_ref).^2,2));
    h_graph(2).YData(iteration) = 4*std(x(xcropidxs),sum(abs(E_ref(xcropidxs,ycropidxs)).^2,2));
    h_graph(3).YData(iteration) = 4*std(y,sum(abs(E_ref).^2,1));
    h_graph(4).YData(iteration) = 4*std(y(ycropidxs),sum(abs(E_ref(xcropidxs,ycropidxs)).^2,1));
    h_graph2.YData(iteration) = magEdiffRMS/maxAbsEmid;
    h_im11.CData(:,iteration) = fidelities;

    if iteration > 1
      h_graph(1).Parent.XLim = [1 iteration];
      h_graph2.Parent.XLim = [1 iteration];
    end
    h_im11.Parent.XLim = [0.5 iteration+0.5];
    drawnow;
  end
  iteration = iteration + 1;
end
toc
fprintf('\b Done\n');

%% If plotProgress was set false, run one additional step to get final fidelities
if ~plotProgress
  parameters.plotProgress = true;
  if calcType == 2
    if useGPU
      [~,~,~,~,~,~,fidelities] = PRiterator_CUDA(E_ref,parameters,frameshifts);
    else
      [~,~,~,~,~,~,fidelities] = PRiterator(E_ref,parameters,frameshifts);
    end
  end
end

%% Make plot of final fidelities
figure(fignum3);clf;
colorvector = zeros(Nz,1);
for i=1:Nz_PR; colorvector(iz_PR_array(i)+1) = 1; end
bar(fidelities,'CData',colorvector,'FaceColor','flat');
colormap(lines(2));
ylim([0.9 1]);
xlim([0.5 Nz+0.5]);
xlabel('Frame');
ylabel('Fidelity (max squared norm of xcorrelation coeffs)');

%% If saving data, calculate and save calculated intensities in the original z planes, as well as retrieved complex E field in reference plane
if saveData
  I_phaseretrieved = zeros(Nx_original,Ny_original,Nz);

  E_ref_freqspace = fft2(E_ref);
  for fidx=1:Nz
    I_full = abs(ifft2(E_ref_freqspace.*prop_kernel(:,:,fidx))).^2;
    I_phaseretrieved(:,:,fidx) = imresize(I_full(1+padding:Nx-padding,1+padding:Ny-padding),[Nx_original Ny_original],'method','bilinear');
  end

  E_ref = imresize(E_ref(1+padding:Nx-padding,1+padding:Ny-padding),[Nx_original Ny_original]);

  fprintf('Saving data to %s...\n',[PathName FileName(1:end-4) '_phaseretrieved.mat']);
  save([PathName FileName(1:end-4) '_phaseretrieved.mat'], 'fidelities', 'E_ref', 'refplaneposition', 'I', 'z_positions', 'lambda', 'pixelpitch_x', 'pixelpitch_y');
end
